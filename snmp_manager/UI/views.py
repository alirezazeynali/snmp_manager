from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views import generic
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import logout

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .SnmpEngine import CommandGeneration
from .SnmpEngine import NotificationReciever
from . import forms
from . import models
from .scripts import upLevelManager, downLevelManager

import json
import os

from .scripts import downLevelManager, upLevelManager


class DashbordView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/Dashbord.html"

    def get(self, request):
        MoM_status = models.SystemConfiguration.objects.all()[0].MoM
        r_trap = models.TrapRecord.objects.all().order_by("-DateTime")[:5]
        r_trap_count = models.TrapRecord.objects.all().count()
        s_trap = models.SendTrapRecord.objects.all().order_by("-DateTime")[:5]
        s_trap_count = models.SendTrapRecord.objects.all().count()
        s_trap_suc = models.SendTrapRecord.objects.filter(
            Response="OK").count()
        if s_trap_count == 0:
            s_trap_d_suc = 0
            s_trap_d_d = 0
        else:
            s_trap_d_suc = (s_trap_suc * 100)/s_trap_count
            s_trap_d_d = 100 - s_trap_d_suc
        total_trap = r_trap_count + s_trap_count
        if r_trap_count == 0:
            r_trap_d = 0
        else:
            r_trap_d = (r_trap_count * 100)/total_trap
        if s_trap_count == 0:
            s_trap_d = 0
        else:
            s_trap_d = (s_trap_count * 100)/total_trap
        return render(request, self.template_name, context={"r_trap": r_trap, "s_trap": s_trap, "r_trap_count": r_trap_count, "s_trap_count": s_trap_count, "r_trap_d": r_trap_d, "s_trap_d": s_trap_d, "s_trap_d_suc": s_trap_d_suc, "s_trap_d_d": s_trap_d_d, "MoM_status": MoM_status})


class MibInitView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/mib_init.html"
    mibs = [""]

    def get(self, request):
        SnmpUsers = models.SnmpUsm.objects.all()
        context = {"SnmpUsers": SnmpUsers}
        return render(request, self.template_name, context)


class WelcomePageView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/welcome.html"

    def get(self, request):
        conf = models.SystemConfiguration.objects.all().count()
        return render(request, self.template_name, context={"data": "welcome", "conf": conf})


class CommandGeneratorView(LoginRequiredMixin, generic.View):
    form_class = forms
    if models.SystemConfiguration.objects.all():
        MoM_status = models.SystemConfiguration.objects.all()[0].MoM
    template_name = "SnmpManager/CommandGenerator.html"
    def get(self, request):

        SnmpUsers = models.SnmpUsm.objects.all()
        context = {"SnmpUsers": SnmpUsers}
        return render(request, self.template_name, context)

    def post(self, request):
        form = forms.CommandForm(request.POST)
        if form.is_valid():
            snmp_method = form.cleaned_data["snmp_method"]
            object_id = form.cleaned_data["object_id"]
            snmp_user = form.cleaned_data["snmp_user"]
            target_IP = form.cleaned_data["target_IP"]
            new_value = form.cleaned_data["new_value"]
            print(new_value)
            if not new_value:
                new_value = None
            if models.SystemConfiguration.objects.all()[0].MoM_Port:
                MoM_Port = models.SystemConfiguration.objects.all()[0].MoM_Port
            snmp_authentication_info = models.SnmpUsm.objects.get(
                SnmpUserName=snmp_user)
            if self.MoM_status == True:
                ViaIP_status = models.SubManager.objects.filter(
                    TargetIP=target_IP)
                if ViaIP_status:
                    ViaIP = models.SubManager.objects.get(
                        TargetIP=target_IP).ViaIP
                    if snmp_authentication_info.SnmpVersion == "version3":

                        sending = upLevelManager.Socket(snmp_method=snmp_method, snmp_version=snmp_authentication_info.SnmpVersion, agent_IP=target_IP, ViaIP=ViaIP, mib_OID=object_id,
                                                        user_name=snmp_authentication_info.SnmpUserName,
                                                        privacy_password=snmp_authentication_info.PrivacyPassword, privacy_protocol=snmp_authentication_info.PrivacyProtocol,
                                                        authentication_password=snmp_authentication_info.AuthenticationPassword,
                                                        authentication_protocol=snmp_authentication_info.AuthenticationProtocol, submanagerPort=MoM_Port, new_value=new_value)
                        output = sending.soc()
                        method = snmp_method
                    else:
                        sending = upLevelManager.Socket(snmp_method=snmp_method,  ViaIP=ViaIP, snmp_version=snmp_authentication_info.SnmpVersion,
                                                        agent_IP=target_IP, mib_OID=object_id, comminuty_string=snmp_authentication_info.CommunityString,
                                                        new_value=new_value, submanagerPort=MoM_Port)
                        output = sending.soc()
                        method = snmp_method
                else:
                    if snmp_method == "get":
                        method = "get"
                        if snmp_authentication_info.SnmpVersion == "version3":
                            snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion, agent_IP=target_IP, mib_OID=object_id,
                                                                               user_name=snmp_authentication_info.SnmpUserName,
                                                                               privacy_password=snmp_authentication_info.PrivacyPassword, privacy_protocol=snmp_authentication_info.PrivacyProtocol,
                                                                               authentication_password=snmp_authentication_info.AuthenticationPassword,
                                                                               authentication_protocol=snmp_authentication_info.AuthenticationProtocol)
                            output = snmp_command.snmpGET()
                        else:
                            snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion,
                                                                               agent_IP=target_IP, mib_OID=object_id, comminuty_string=snmp_authentication_info.CommunityString)
                            output = snmp_command.snmpGET()
                    if snmp_method == "set":
                        method = "set"
                        if snmp_authentication_info.SnmpVersion == "version3":
                            snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion, agent_IP=target_IP, mib_OID=object_id,
                                                                               user_name=snmp_authentication_info.SnmpUserName,
                                                                               privacy_password=snmp_authentication_info.PrivacyPassword, privacy_protocol=snmp_authentication_info.PrivacyProtocol,
                                                                               authentication_password=snmp_authentication_info.AuthenticationPassword,
                                                                               authentication_protocol=snmp_authentication_info.AuthenticationProtocol,
                                                                               new_value=new_value)
                            output = snmp_command.snmpSET()
                        else:
                            snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion,
                                                                               agent_IP=target_IP, mib_OID=object_id, comminuty_string=snmp_authentication_info.CommunityString,
                                                                               new_value=new_value)
                            output = snmp_command.snmpGET()
                return render(request, template_name="SnmpManager/CommandResult.html", context={"output": output, "method": method, "object_id": object_id, "target_ip": target_IP, "snmp_user": snmp_user})
            else:
                if snmp_method == "get":
                    method = "get"
                    if snmp_authentication_info.SnmpVersion == "version3":
                        snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion, agent_IP=target_IP, mib_OID=object_id,
                                                                           user_name=snmp_authentication_info.SnmpUserName,
                                                                           privacy_password=snmp_authentication_info.PrivacyPassword, privacy_protocol=snmp_authentication_info.PrivacyProtocol,
                                                                           authentication_password=snmp_authentication_info.AuthenticationPassword,
                                                                           authentication_protocol=snmp_authentication_info.AuthenticationProtocol)
                        output = snmp_command.snmpGET()
                    else:
                        snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion,
                                                                           agent_IP=target_IP, mib_OID=object_id, comminuty_string=snmp_authentication_info.CommunityString)
                        output = snmp_command.snmpGET()
                if snmp_method == "set":
                    method = "set"
                    if snmp_authentication_info.SnmpVersion == "version3":
                        snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion, agent_IP=target_IP, mib_OID=object_id,
                                                                           user_name=snmp_authentication_info.SnmpUserName,
                                                                           privacy_password=snmp_authentication_info.PrivacyPassword, privacy_protocol=snmp_authentication_info.PrivacyProtocol,
                                                                           authentication_password=snmp_authentication_info.AuthenticationPassword,
                                                                           authentication_protocol=snmp_authentication_info.AuthenticationProtocol,
                                                                           new_value=new_value)
                        output = snmp_command.snmpSET()
                    else:
                        snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion,
                                                                           agent_IP=target_IP, mib_OID=object_id, comminuty_string=snmp_authentication_info.CommunityString,
                                                                           new_value=new_value)
                        output = snmp_command.snmpGET()
                if models.SystemConfiguration.objects.all():
                    MoM_status = models.SystemConfiguration.objects.all()[
                        0].MoM
                else:
                    MoM_status = None
                return render(request, template_name="SnmpManager/CommandResult.html", context={"output": output, "method": method, "object_id": object_id, "target_ip": target_IP, "snmp_user": snmp_user, "MoM_status": MoM_status})


class CommandGetnextView(LoginRequiredMixin, generic.View):
    def post(self, request):
        form = forms.CommandForm(request.POST)
        if form.is_valid():
            object_id = form.cleaned_data["object_id"]
            snmp_user = form.cleaned_data["snmp_user"]
            target_IP = form.cleaned_data["target_IP"]
            snmp_authentication_info = models.SnmpUsm.objects.get(
                SnmpUserName=snmp_user)
            if snmp_authentication_info.SnmpVersion == "version3":
                snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion, agent_IP=target_IP, mib_OID=object_id,
                                                                   user_name=snmp_authentication_info.SnmpUserName,
                                                                   privacy_password=snmp_authentication_info.PrivacyPassword, privacy_protocol=snmp_authentication_info.PrivacyProtocol,
                                                                   authentication_password=snmp_authentication_info.AuthenticationPassword,
                                                                   authentication_protocol=snmp_authentication_info.AuthenticationProtocol)
                output = snmp_command.snmpGETNEXT(str(object_id))
            else:
                snmp_command = CommandGeneration.CommandGenerators(snmp_version=snmp_authentication_info.SnmpVersion,
                                                                   agent_IP=target_IP, mib_OID=object_id, comminuty_string=snmp_authentication_info.CommunityString)
                output = snmp_command.snmpGETNEXT(str(object_id))
        else:
            HttpResponse("nashod")
        if models.SystemConfiguration.objects.all():
            MoM_status = models.SystemConfiguration.objects.all()[
                0].MoM
        else:
            MoM_status = None

        return render(request, template_name="SnmpManager/CommandResult.html", context={"output": output, "method": "get", "object_id": object_id, "target_ip": target_IP, "snmp_user": snmp_user, "MoM_status": MoM_status})


class SystemConfigurationView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/SystemConf.html"

    def get(self, request):
        conf = models.SystemConfiguration.objects.all()
        if conf:
            return render(request, self.template_name, {"conf": conf})
        else:
            return render(request, self.template_name)

    def post(self, request):
        form = forms.SysConfForm(request.POST)

        if form.is_valid():
            system_email = form.cleaned_data["system_email"]
            system_email_password = form.cleaned_data["system_email_password"]
            system_email_protocol = form.cleaned_data["system_email_protocol"]
            system_ip = form.cleaned_data["system_ip"]
            system_name = form.cleaned_data["system_name"]
            MoM = form.cleaned_data["MoM"]
            MoM_IP = form.cleaned_data["MoM_IP"]
            MoM_Port = form.cleaned_data["MoM_Port"]
            if MoM_Port:
                MoM_Port = int(MoM_Port)
            else:
                MoM_Port = None

            check = models.SystemConfiguration.objects.all().count()
            if check == 1:
                conf = models.SystemConfiguration.objects.all()
                for c in conf:
                    c.SystemName = system_name
                    c.SystemEmail = system_email
                    c.SystemEmailPassword = system_email_password
                    c.SystemEmailProtocol = system_email_protocol
                    c.SystemIp = system_ip
                    c.MoM = MoM
                    c.MoM_IP = MoM_IP
                    c.MoM_Port = int(MoM_Port)
                    c.save()
            else:
                if MoM_Port:
                    MoM_Port = int(MoM_Port)
                else:
                    MoM_Port = None
                instance = models.SystemConfiguration.objects.create(SystemName=system_name, SystemEmail=system_email, SystemEmailPassword=system_email_password,
                                                                     SystemEmailProtocol=system_email_protocol, SystemIp=system_ip, MoM=MoM, MoM_IP=MoM_IP, MoM_Port=MoM_Port)

            return render(request, "SnmpManager/message.html", context={"session": "Ok", "message": "you modify system configuration successfully."})
        else:
            return render(request, "SnmpManager/message.html", context={"session": "Failed", "message": "your request is failed , please try again."})


class TrapReceiverView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/TrapReceiver.html"

    def get(self, request):
        TrapProfile = models.RecievedTrapConfiguration.objects.all()
        context = {"TrapProfile": TrapProfile}
        return render(request, self.template_name, context)

    def post(self, request):
        form = forms.RTrapForm(request.POST)
        if form.is_valid():
            trap_profile = form.cleaned_data["trap_profile"]

            profile = models.RecievedTrapConfiguration.objects.get(
                SenderIP=trap_profile)
            ip = models.SystemConfiguration.objects.all()[0].SystemIp

            notification = NotificationReciever.NotificationReciever(profile.SnmpUser.SnmpVersion, ip, user_name=profile.SnmpUser.SnmpUserName, job_id=profile.id, privacy_password=profile.SnmpUser.PrivacyPassword, privacy_protocol=profile.SnmpUser.PrivacyProtocol,
                                                                     authentication_password=profile.SnmpUser.AuthenticationPassword, authentication_protocol=profile.SnmpUser.AuthenticationProtocol, comminuty_string=profile.SnmpUser.CommunityString,
                                                                     security_engine_id=profile.EngineID, comminuty_index=profile.SnmpUser.CommunityIndex, port=profile.ReceiverPort)
            notification.start()

            return redirect("trap_listner", snmpuser=profile.SnmpUser.SnmpUserName, snmpver=profile.SnmpUser.SnmpVersion)


class TrapListnerView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/TrapListner.html"

    def get(self, request, *args, **kwargs):
        USM = models.SnmpUsm.objects.get(
            SnmpUserName=self.kwargs["snmpuser"], SnmpVersion=self.kwargs["snmpver"])
        context = models.TrapRecord.objects.filter(
            SnmpUsm=USM).order_by("-DateTime")

        return render(request, self.template_name, context={"watch": context, "user": self.kwargs["snmpuser"]})


class TrapSenderView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/TrapSender.html"

    def get(self, request):
        sendProfiles = models.SendTrapConfiguration.objects.all()
        return render(request, self.template_name, {"Profiles": sendProfiles})

    def post(self, request):
        form = forms.STrapForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data["trap_profile"]
            finder = data.split("|")
            return redirect("trap_sender_res", targetip=finder[0], oid=finder[1])
        else:
            return render(request, "SnmpManager/message.html", context={"session": "Failed", "message": "your request is failed , please try again."})


class LogoutView(LoginRequiredMixin, generic.View):
    template_name = "registration/logout.html"

    def get(self, request):
        logout(request)
        return render(request, self.template_name)


class TrapSenderResView(LoginRequiredMixin, generic.View):
    template_name = "SnmpManager/TrapSenderRes.html"

    def get(self, request, *args, **kwargs):
        sendProfile = models.SendTrapConfiguration.objects.get(
            TargetIP=self.kwargs["targetip"], ObjectID=self.kwargs["oid"])
        send_traps = models.SendTrapRecord.objects.filter(
            SendTrapProfile=sendProfile)
        return render(request, self.template_name, {"traps": send_traps})


class RootView(generic.View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect("dashbord")
        else:
            return redirect("login")


class SocketInitial(generic.View):
    def get(self, request):
        print("run1")
        socket = downLevelManager.Socket()
        socket.start()
        return redirect("dashbord")


class ProfileView( APIView):
    template_name = "SnmpManager/Profile.html"

    def get(self, request, *args, **kwargs):
        current_user = models.User.objects.get(username="admin")
        data = {}
        if models.Profile.objects.filter(user_profile=current_user):
            profiles = models.Profile.objects.all().filter(user_profile=current_user)

            print(profiles)
            pulling_records = []
            trap_records = []
            for profile in profiles:
                print("ssssssssssssssssssssssssssssssssssssssssssssss")
                if models.PullingRecord.objects.filter(MibOID=profile.pulling_oid):
                    pulling_record = models.PullingRecord.objects.all().get(
                        MibOID=profile.pulling_oid, AgentIP=profile.IP)
                    print(pulling_record.Value)
                    if "= " in pulling_record.Value:
                        value = pulling_record.Value.split("= ")[1]
                        print(value)
                    else:
                        value = trap_record.Value
                    record_view = {"updated_at": str(pulling_record.DateTime), "critical_value": profile.critical_value, "data_type": profile.object_type, "value": value, "method" : profile.record_method, "critical" : profile.critical_value, "name" : pulling_record.MibOID, "ip": pulling_record.AgentIP}
                pulling_records.append(record_view)
                print("data",pulling_records)
        data.update({
            "pulling" : pulling_records
        })

        return Response(data=data, status=status.HTTP_200_OK)




    #
    # if models.TrapRecord.objects.filter(Name=profile.trap_oid):
    #     print("here")
    #     trap_record = models.TrapRecord.objects.all().get(
    #         trap_oid=profile.trap_oid)
    #     if "= " in trap_record.Value:
    #         value = trap_record.Value.split("= ")[1]
    #     else:
    #         value = trap_record.Value
    #     record_view = {"record": trap_record, "critical_value": profile.critical_value, "data_type": profile.object_type, "value": value,"method" : profile.record_method, "critical" : profile.critical_value}
    #     trap_records.append(record_view)
    #     data.append({"pulling_records": pulling_records, "trap_records": trap_records})



