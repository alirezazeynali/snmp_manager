from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.SnmpUsm)
class SnmpUsmAdmin(admin.ModelAdmin):
    list_display = ('SnmpUserName', 'SnmpVersion')

@admin.register(models.SendTrapConfiguration)
class SendTrapConfigurationAdmin(admin.ModelAdmin):
    pass

@admin.register(models.RecievedTrapConfiguration)
class RecievedTrapConfigurationAdmin(admin.ModelAdmin):
    pass

@admin.register(models.SystemConfiguration)
class SystemConfigurationAdmin(admin.ModelAdmin):
    pass

@admin.register(models.ManagerOfManagerInfo)
class ManagerOfManagerInfoAdmin(admin.ModelAdmin):
    pass    
@admin.register(models.SubManager)
class SubManagerAdmin(admin.ModelAdmin):
    pass

@admin.register(models.OIDTiming)
class OIDTimingAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass