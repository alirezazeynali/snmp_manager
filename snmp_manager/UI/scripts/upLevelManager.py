import json
import socket
import threading

# snmp_version = snmp_authentication_info.SnmpVersion, agent_IP = target_IP, ViaIP = ViaIP, mib_OID = object_id,
#                          user_name = snmp_authentication_info.SnmpUserName,
#                          privacy_password = snmp_authentication_info.PrivacyPassword, privacy_protocol = snmp_authentication_info.PrivacyProtocol,
#                          authentication_password = snmp_authentication_info.AuthenticationPassword,
#                          authentication_protocol = snmp_authentication_info.AuthenticationProtocol


class Socket():
    def __init__(self, snmp_method, snmp_version, agent_IP, ViaIP, submanagerPort, mib_OID, user_name=None, privacy_password=None, privacy_protocol=None, authentication_password=None, authentication_protocol=None, comminuty_string=None, new_value=None):

        self.snmp_method = snmp_method
        self.snmp_version = snmp_version
        self.agent_IP = agent_IP
        self.ViaIP = ViaIP
        self.mib_OID = mib_OID
        self.user_name = user_name
        self.privacy_password = privacy_password
        self.privacy_protocol = privacy_protocol
        self.authentication_password = authentication_password
        self.authentication_protocol = authentication_protocol
        self.comminuty_string = comminuty_string
        self.new_value = new_value
        self.submanagerPort = submanagerPort

    def soc(self):
        serverIP = self.ViaIP
        port = self.submanagerPort
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((serverIP, port))
        if self.snmp_version == 'version3':
            privacy_data = {
                "privacy_protocol": self.privacy_protocol,
                "privacy_password": self.privacy_password
            }
            authentication_data = {
                "authentication_protocol": self.authentication_protocol,
                "authentication_password": self.authentication_password
            }
        else:
            privacy_data = None,
            authentication_data = None
        data = {
            "snmp_method": self.snmp_method,
            "snmp_version": self.snmp_version,
            "snmp_data": {
                "user_name": self.user_name,
                "new_value": self.new_value,
                "privacy_data": privacy_data,
                "authentication_data": authentication_data,
                "agent_IP": self.agent_IP,
                "Mib_Oid": self.mib_OID,
                "comminuty_string": self.comminuty_string
            }
        }
        outgoingJson = json.dumps(data)
        client.send(bytes(outgoingJson, encoding="utf-8"))
        while True:

            in_data = client.recv(2048)
            in_data = in_data.decode("utf-8")

            break

        client.close()
        return in_data
# view ro dorost kon
