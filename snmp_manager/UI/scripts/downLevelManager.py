import threading
import json
import socket
import datetime
from ..SnmpEngine import CommandGeneration
from ..models import SystemConfiguration
from ..models import PullingRecord
from django.utils.timezone import now
# snmp_version, agent_IP, mib_OID, user_name = None, comminuty_string = None,
#  privacy_password = None, privacy_protocol = None, authentication_password = None, authentication_protocol = None, new_value = None


class ManagerOfManagerTheard(threading.Thread):
    def __init__(self, ManagerOfManagerAddress, ManagerOfManagerSocket):
        threading.Thread.__init__(self)
        self.MoMSocket = ManagerOfManagerSocket

    def run(self):

        incomingJson = self.MoMSocket.recv(2048)

        incomingDic = json.loads(incomingJson, encoding='utf-8')
        print(incomingDic)
        print(incomingDic["snmp_data"]["new_value"])
        command = CommandGeneration.CommandGenerators(snmp_version=incomingDic["snmp_version"], agent_IP=incomingDic["snmp_data"]["agent_IP"],
                                                      mib_OID=incomingDic["snmp_data"][
                                                          "Mib_Oid"], user_name=incomingDic["snmp_data"]["user_name"],
                                                      comminuty_string=incomingDic["snmp_data"]["comminuty_string"], privacy_protocol=incomingDic[
                                                          "snmp_data"]["privacy_data"]["privacy_protocol"],
                                                      privacy_password=incomingDic["snmp_data"]["privacy_data"]["privacy_password"], authentication_protocol=incomingDic[
                                                          "snmp_data"]["authentication_data"]["authentication_protocol"],
                                                      authentication_password=incomingDic["snmp_data"]["authentication_data"]["authentication_password"], new_value=incomingDic["snmp_data"]["new_value"])
        if incomingDic["snmp_method"] == 'get':
            record = PullingRecord.objects.filter(
                SnmpMethod='get', MibOID=incomingDic["snmp_data"]["Mib_Oid"], AgentIP=incomingDic["snmp_data"]["agent_IP"])
            if record:

                record = PullingRecord.objects.get(
                    SnmpMethod='get', MibOID=incomingDic["snmp_data"]["Mib_Oid"], AgentIP=incomingDic["snmp_data"]["agent_IP"])
                if now() - record.DateTime < record.TimeToLive:
                    print(bytes(record.Value, "utf-8"))
                    self.MoMSocket.send(bytes(record.Value, "utf-8"))
                else:

                    output = command.snmpGET()
                    self.MoMSocket.send(bytes(output, "utf-8"))
            else:

                output = command.snmpGET()
                self.MoMSocket.send(bytes(output, "utf-8"))
        if incomingDic["snmp_method"] == 'set':
            output = command.snmpSET()
            self.MoMSocket.send(bytes(output, "utf-8"))


class Socket(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        self.ip = SystemConfiguration.objects.all()[0].SystemIp
        self.port = SystemConfiguration.objects.all()[0].MoM_Port
        print("run")
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind((self.ip, self.port))
        while True:
            server.listen(1)
            ManagerOfManagerSocket, ManagerOfManagerAddress = server.accept()
            newthread = ManagerOfManagerTheard(
                ManagerOfManagerAddress, ManagerOfManagerSocket)
            newthread.start()
