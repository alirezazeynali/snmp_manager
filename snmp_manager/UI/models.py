from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from django.utils import timezone


class SnmpUsm(models.Model):
    VERSION_CHOICE = (
        ("version1", "Version 1"),
        ("version2c", "Version 2.C"),
        ("version3", "Version 3")
    )
    PRIVACY_PROTOCOL_CHOICE = (
        ("usmDESPrivProtocol", "usmDESPrivProtocol"),
        ("usm3DESEDEPrivProtocol", "usm3DESEDEPrivProtocol"),
        ("usmAesCfb128Protocol", "usmAesCfb128Protocol"),
        ("usmAesCfb192Protocol", "usmAesCfb192Protocol"),
        ("usmAesCfb256Protocol", "usmAesCfb256Protocol"),
        ("usmNoPrivProtocol", "usmNoPrivProtocol")
    )
    AUTHENTICAION_PROTOCOL_CHOICE = (
        ("usmHMACMD5AuthProtocol", "usmHMACMD5AuthProtocol"),
        ("usmHMACSHAAuthProtocol", "usmHMACSHAAuthProtocol"),
        ("usmHMAC128SHA224AuthProtocol", "usmHMAC128SHA224AuthProtocol"),
        ("usmHMAC192SHA256AuthProtocol", "usmHMAC192SHA256AuthProtocol"),
        ("usmHMAC256SHA384AuthProtocol", "usmHMAC256SHA384AuthProtocol"),
        ("usmHMAC384SHA512AuthProtocol", "usmHMAC384SHA512AuthProtocol"),
        ("usmNoAuthProtocol", "usmNoAuthProtocol")
    )
    SnmpUserName = models.CharField(max_length=50)
    SnmpVersion = models.CharField(
        choices=VERSION_CHOICE, default="version3", max_length=10)
    CommunityString = models.CharField(max_length=250, null=True, blank=True)
    PrivacyPassword = models.CharField(blank=True, null=True, max_length=20)
    PrivacyProtocol = models.CharField(
        choices=PRIVACY_PROTOCOL_CHOICE, default="usmNoPrivProtocol", blank=True, null=True, max_length=30)
    AuthenticationPassword = models.CharField(
        blank=True, null=True, max_length=20)
    AuthenticationProtocol = models.CharField(
        choices=AUTHENTICAION_PROTOCOL_CHOICE, default="usmNoAuthProtocol", blank=True, null=True, max_length=30)
    CommunityIndex = models.CharField(max_length=100, default="public")
    Creator = models.ForeignKey(User, models.CASCADE)

    class Meta:
        ordering = ["-SnmpVersion"]

    def __str__(self):
        if self.CommunityString:
            return "User Name : {} | Version : {} | Community String : {}".format(self.SnmpUserName, self.SnmpVersion, self.CommunityString)
        else:
            return "User Name : {} | Version : {} | Privacy Protocol : {} | Authentication Protocol : {}".format(self.SnmpUserName, self.SnmpVersion, self.PrivacyProtocol, self.AuthenticationProtocol)


class SystemConfiguration(models.Model):
    PROTOCOL_CHOICE = (
        ("pop3", "POP 3"),
        ("smtp", "SMTP")
    )
    SystemName = models.CharField(max_length=70)
    SystemEmail = models.CharField(max_length=70)
    SystemEmailPassword = models.CharField(max_length=20)
    SystemEmailProtocol = models.CharField(
        choices=PROTOCOL_CHOICE, default="smtp", max_length=5)
    SystemIp = models.CharField(
        max_length=15, default="192.168.1.100", null=True)
    MoM = models.BooleanField(default=False)
    MoM_IP = models.CharField(max_length=15, null=True, blank=True)
    MoM_Port = models.IntegerField(null=True)
    # def save(self, *args, **kwargs):
    #     if SystemConfiguration.objects.count() > 0:
    #         raise PermissionError

    #     super(SystemConfiguration, self).save(*args, **kwargs)
    class Meta:
        permissions = (
            ("can_see_and_edit", "psuedo"),
        )

    def __str__(self):
        if self.MoM == False:
            return "System Name : {} | System IP : {} | Manager of manager status : {}".format(self.SystemName, self.SystemIp, self.MoM)
        else:
            return "System Name : {} | System IP : {} | Manager of manager status : {} | Manager of manager IP : {}".format(self.SystemName, self.SystemIp, self.MoM, self.MoM_IP)


class RecievedTrapConfiguration(models.Model):
    SenderIP = models.CharField(max_length=15)
    ReceiverPort = models.IntegerField(default=162, null=True, blank=True)
    SnmpUser = models.OneToOneField(SnmpUsm, on_delete=models.CASCADE)
    EngineID = models.CharField(max_length=150)

    def __str__(self):
        return "Sender IP : {} | Receiver Port : {} | Snmp User : {} | Engine ID : {}".format(
            self.SenderIP,
            self.ReceiverPort,
            self.SnmpUser.SnmpUserName,
            self.EngineID
        )


class TrapRecord(models.Model):
    MOM_CHOICE = (
        ("yes", "yes"),
        ("no", "no"),
    )
    SnmpUsm = models.ForeignKey(SnmpUsm, on_delete=models.CASCADE)
    Name = models.CharField(max_length=50)
    Value = models.CharField(max_length=150)
    contextEngineID = models.CharField(max_length=150)
    ContextName = models.CharField(max_length=50)
    DateTime = models.DateTimeField(default=timezone.now())
    SenderIP = models.CharField(max_length=15)
    ManagerOfManager = models.CharField(
        max_length=3, choices=MOM_CHOICE, default="no")

    def __str__(self):
        return "User Name : {} | Name : {} | Value : {} | ContextName : {} | DateTime : {} | IP : {}".format(
            self.SnmpUsm.SnmpUserName,
            self.Name,
            self.Value,
            self.ContextName,
            self.DateTime,
            self.SenderIP
        )


class SendTrapConfiguration(models.Model):
    TargetIP = models.CharField(max_length=15)
    ObjectID = models.CharField(max_length=50)
    SenderPort = models.IntegerField()
    ContextEngineID = models.CharField(max_length=150)
    RecievedTrap = models.OneToOneField(
        RecievedTrapConfiguration, models.CASCADE)

    def __str__(self):
        return "Target IP : {} | OID : {} | Context Engine ID : {} | Source IP : {} | USM : {} ".format(
            self.TargetIP,
            self.ObjectID,
            self.ContextEngineID,
            self.RecievedTrap.SenderIP,
            self.RecievedTrap.SnmpUser.SnmpUserName
        )


class SendTrapRecord(models.Model):
    Name = models.CharField(max_length=50)
    Value = models.CharField(max_length=150)
    DateTime = models.DateTimeField(default=timezone.now(), blank=True)
    SendTrapProfile = models.ForeignKey(SendTrapConfiguration, models.CASCADE)
    Response = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return "Name : {} | Value : {} | DateTime : {} | Source IP : {} | Target IP : {} | Context Engine ID : {}".format(
            self.Name,
            self.Value,
            self.DateTime,
            self.SendTrapProfile.RecievedTrap.SenderIP,
            self.SendTrapProfile.TargetIP,
            self.SendTrapProfile.ContextEngineID
        )


class ManagerOfManagerInfo(models.Model):
    SourceIP = models.CharField(max_length=15)
    ContextEngineID = models.CharField(max_length=150)
    RecievedTrapConf = models.ForeignKey(
        RecievedTrapConfiguration, models.CASCADE)


class SubManager(models.Model):
    TargetIP = models.CharField(max_length=15)
    ViaIP = models.CharField(max_length=15)


class PullingRecord(models.Model):
    METHODCHOICE = (
        ('get', 'get'),
        ('set', 'set')
    )
    SnmpMethod = models.CharField(
        choices=METHODCHOICE, default='get', max_length=3)
    MibOID = models.CharField(max_length=150)
    Value = models.CharField(max_length=150)
    DateTime = models.DateTimeField(default=timezone.now())
    SnmpUsm = models.ForeignKey(SnmpUsm, on_delete=models.CASCADE)
    TimeToLive = models.DurationField(default=timedelta(minutes=120))
    AgentIP = models.CharField(max_length=15)


class OIDTiming(models.Model):
    MibOID = models.CharField(max_length=150)
    TimeToLive = models.IntegerField()

class Profile(models.Model):
    RECORDMETHOD = (
        ("Pull", "Pull"),
        ("Trap", "Trap")
    )
    OBJTYPE = (
        ("Integer", "Integer"),
        ("Integer32", "Integer32"),
        ("UInteger32", "UInteger32"),
        ("Octet-String", "Octet-String"),
        ("Object-Identifier", "Object-Identifier"),
        ("Bit-String", "Bit-String"),
        ("IpAddress", "IpAddress"),
        ("Counter32", "Counter32"),
        ("Counter64", "Counter64"),
        ("Gauge32", "Gauge32"),
        ("TimeTicks", "TimeTicks"),
        ("Opaque", "Opaque"),
        ("NsapAddress", "NsapAddress")
    )
    user_profile = models.ForeignKey(User, on_delete=models.CASCADE)
    record_method = models.CharField(max_length=4, choices=RECORDMETHOD)
    object_type = models.CharField(max_length=20, choices=OBJTYPE)
    critical_value = models.CharField(max_length=250)
    pulling_oid = models.CharField(max_length=150, null=True, blank=True)
    trap_oid = models.CharField(max_length=150, null=True, blank=True)
    IP = models.CharField(max_length=16, blank=True, null=True)