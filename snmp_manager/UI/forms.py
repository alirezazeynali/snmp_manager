from django import forms

class CommandForm(forms.Form):
    snmp_method = forms.CharField(max_length=10)
    object_id = forms.CharField(max_length=10000)
    snmp_user = forms.CharField(max_length=200)
    target_IP = forms.CharField(max_length=15)
    new_value = forms.CharField(max_length=500 , required = False)
    object_method = forms.CharField(max_length=10, required = False)
class RTrapForm(forms.Form):
    trap_profile = forms.CharField(max_length=150)

class SysConfForm(forms.Form):
    system_email = forms.CharField(max_length=100)
    system_email_password = forms.CharField(min_length=8)
    system_email_protocol = forms.CharField(max_length=10)
    system_ip = forms.CharField(max_length=15)
    system_name = forms.CharField(max_length=25)
    MoM = forms.BooleanField(required=False)
    MoM_IP = forms.CharField(required=False)
    MoM_Port = forms.CharField(required=False)
class STrapForm(forms.Form):
    trap_profile = forms.CharField(max_length=1100)

    
