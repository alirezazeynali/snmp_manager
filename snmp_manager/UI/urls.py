from django.urls import path
from . import views
urlpatterns = [
    path("welcome/", views.WelcomePageView.as_view(), name = "welcome_page"),
    path("dashboard/", views.DashbordView.as_view(), name = "dashbord"),
    path("mib_init/", views.MibInitView.as_view(), name = "mib_init"),
    path("command_generator/", views.CommandGeneratorView.as_view(), name = "command_generator"),
    path("command_getnext/", views.CommandGetnextView.as_view(), name = "command_getnext"),
    path("trap_configuration/", views.TrapReceiverView.as_view(), name = "trap_configuration"),
    path("trap_listner/<str:snmpuser>/<str:snmpver>", views.TrapListnerView.as_view(), name = "trap_listner"),
    path("system_configuration/", views.SystemConfigurationView.as_view(), name = "system_configuration"),
    path("logout/", views. LogoutView.as_view(), name = "loggedout"),
    path("send_trap_configuration/", views.TrapSenderView.as_view(),name = "trap_sender"),
    path("send_trap_res/<str:targetip>/<str:oid>", views.TrapSenderResView.as_view(), name = "trap_sender_res"),
    path("", views.RootView.as_view(), name = "root"),
    path("socket/", views.SocketInitial.as_view(), name = "socket"),
    path("profile/", views.ProfileView.as_view(), name="profile")
]