from pysnmp.entity import engine
from pysnmp.entity.config import *
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.entity.rfc3413 import ntfrcv
from pysnmp.proto.api import v2c
from .dependency import convertor
import json
import threading
from .. import models
from .dependency import hierarchy
from pysnmp.carrier.asyncore.dispatch import AsyncoreDispatcher


class jobs(threading.Thread):
    def __init__(self, snmpengine, job_id):
        threading.Thread.__init__(self)
        self.snmpengine = snmpengine
        self.job_id = job_id

    def run(self):
        self.snmpengine.transportDispatcher.jobStarted(self.job_id)
        try:
            self.snmpengine.transportDispatcher.runDispatcher(self.job_id)
        except:
            self.snmpengine.transportDispatcher.closeDispatcher(self.job_id)
            raise


class NotificationReciever(threading.Thread):
    def __init__(self, snmp_version, listening_ip, job_id, user_name=None, comminuty_index=None, comminuty_string=None, privacy_password=None, privacy_protocol=None, authentication_password=None, authentication_protocol=None, new_value=None, port=162, security_engine_id=None, status=True):
        threading.Thread.__init__(self)
        self.userName = user_name
        self.snmpVer = snmp_version
        self.privPass = privacy_password
        self.privProtocol = convertor.privacy_convertor(privacy_protocol)
        self.authPass = authentication_password
        self.authProtocol = convertor.authentication_convertor(
            authentication_protocol)
        self.ip = listening_ip
        self.port = port
        self.comminutyString = comminuty_string
        self.securityEngineId = security_engine_id
        self.status = status
        self.comminutyIndex = comminuty_index
        self.job_id = job_id

    def run(self):

        MoM_status = models.SystemConfiguration.objects.all()[0].MoM
        snmpEngine = engine.SnmpEngine()
        addTransport(
            snmpEngine,
            udp.domainName,
            udp.UdpTransport().openServerMode((self.ip, self.port))
        )
        if self.securityEngineId:
            if self.snmpVer == "version3":
                addV3User(
                    snmpEngine, self.userName,
                    self.authProtocol, self.authPass,
                    self.privProtocol, self.privPass,
                    securityEngineId=v2c.OctetString(
                        hexValue=self.securityEngineId)
                )
            else:
                addV1System(snmpEngine, self.comminutyIndex,
                            self.comminutyString)
            snmpVersion = self.snmpVer
            snmpUserName = self.userName

            def cbFun(snmpEngine, stateReference, contextEngineId, contextName, varBinds, cbCtx):
                execContext = snmpEngine.observer.getExecutionContext(
                    'rfc3412.receiveMessage:request')
                USM = models.SnmpUsm.objects.get(
                    SnmpUserName=snmpUserName, SnmpVersion=snmpVersion)
                TrapConfiguration = models.RecievedTrapConfiguration.objects.filter(
                    SenderIP=execContext["transportAddress"][0], SnmpUser=USM)

                if TrapConfiguration:
                    if MoM_status == True:
                        TrapConfiguration = models.RecievedTrapConfiguration.objects.get(
                            SenderIP=execContext["transportAddress"][0], SnmpUser=USM)

                        MoM_info = models.ManagerOfManagerInfo.objects.filter(
                            RecievedTrapConf=TrapConfiguration, ContextEngineID=contextEngineId)
                        if MoM_info:

                            MoM_info = models.ManagerOfManagerInfo.objects.get(
                                RecievedTrapConf=TrapConfiguration, ContextEngineID=contextEngineId)
                            OidValue = {}
                            for name, val in varBinds:
                                models.TrapRecord.objects.create(SnmpUsm=USM, Name=name.prettyPrint(), Value=val.prettyPrint(
                                ), ContextName=contextName, SenderIP=MoM_info.SourceIP, ManagerOfManager="yes")
                                OidValue.update(
                                    {name.prettyPrint(): val.prettyPrint()})
                            x = hierarchy.Checker({"SenderIP": execContext["transportAddress"][0], "Authentication": USM, "EngineID": self.securityEngineId,
                                                   "ContextEngineID": contextEngineId.prettyPrint(),
                                                   "ContextName": contextName.prettyPrint(),
                                                   "OidValue": OidValue
                                                   })
                            x.start()

                    OidValue = {}

                    for name, val in varBinds:
                        models.TrapRecord.objects.create(SnmpUsm=USM, Name=name.prettyPrint(), Value=val.prettyPrint(
                        ), ContextName=contextName, SenderIP=execContext["transportAddress"][0])
                        OidValue.update(
                            {name.prettyPrint(): val.prettyPrint()})

                    x = hierarchy.Checker({"SenderIP": execContext["transportAddress"][0], "Authentication": USM, "EngineID": self.securityEngineId,
                                           "ContextEngineID": contextEngineId.prettyPrint(),
                                           "ContextName": contextName.prettyPrint(),
                                           "OidValue": OidValue
                                           })
                    x.start()
                else:
                    pass
        ntfrcv.NotificationReceiver(snmpEngine, cbFun)
        y = jobs(snmpengine=snmpEngine, job_id=self.job_id)
        y.start()
