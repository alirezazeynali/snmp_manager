from pysnmp.hlapi import *
from .dependency import convertor
from .. import models

import os
import threading
def authentication_convertorC(auth_protocol):
    if auth_protocol == usmHMACMD5AuthProtocol:
        return "MD5"
    if auth_protocol == usmHMACSHAAuthProtocol:
        return "SHA"
    if auth_protocol == usmHMAC128SHA224AuthProtocol:
        return "SHA"
    if auth_protocol == usmHMAC192SHA256AuthProtocol:
        return "SHA"
    if auth_protocol == usmHMAC256SHA384AuthProtocol:
        return "SHA"
    if auth_protocol == usmHMAC384SHA512AuthProtocol:
        return "SHA"
    
def privacy_convertorC(priv_protocol):
    if priv_protocol == usmDESPrivProtocol:
        return  "DES"
    if priv_protocol == usm3DESEDEPrivProtocol:
        return  "DES"
    if priv_protocol == usmAesCfb128Protocol:
        return  "AES"
    if priv_protocol == usmAesCfb192Protocol:
        return  "AES"
    if priv_protocol == usmAesCfb256Protocol:
        return  "AES"
    

def con(priv , auth):
    if auth == usmNoAuthProtocol :
        return "noAuthNoPriv"
    else:
        if priv == usmNoPrivProtocol:
            return "authNoPriv"
        else :
            return "authPriv"
class NotificationOrginator(threading.Thread):
    def __init__(self, send_trap_profile, name, value, cname):
        threading.Thread.__init__(self)
        self.__profile = send_trap_profile
        self.target_ip = send_trap_profile.TargetIP
        self.__snmp_version = send_trap_profile.RecievedTrap.SnmpUser.SnmpVersion
        self.name = name
        self.value = value
        self.port = send_trap_profile.SenderPort
        if self.__snmp_version == "version3":
            self.__user_name = send_trap_profile.RecievedTrap.SnmpUser.SnmpUserName
            self.__privacy_protocol = convertor.privacy_convertor(send_trap_profile.RecievedTrap.SnmpUser.PrivacyProtocol)
            self.__privacy_password = send_trap_profile.RecievedTrap.SnmpUser.PrivacyPassword
            self.__authentication_protocol = convertor.authentication_convertor(send_trap_profile.RecievedTrap.SnmpUser.AuthenticationProtocol)
            self.__authentication_password = send_trap_profile.RecievedTrap.SnmpUser.AuthenticationPassword
            self.__context_engine_id = send_trap_profile.ContextEngineID
            self.__context_name = cname
            self.__security_engine_id = send_trap_profile.RecievedTrap.EngineID
        else :
            if self.__snmp_version == "version1":
                self.__mp_model =0
            else :
                self.__mp_model = 1
            self.__community_string = send_trap_profile.RecievedTrap.SnmpUsm.CommunityString
            self.__context_name = send_trap_profile.RecievedTrap.ContextName    
    def run(self):
        print("run")
        if self.__snmp_version == "version3":
            # try:
                print("xxxx")
                print(self.__security_engine_id, self.__context_engine_id)
                os.system("snmptrap -v3 -e {} -E {} -l {} -u {} -X {} -A {} -a {} -x {} {}:{} {} {}".format(self.__security_engine_id, self.__context_engine_id,
                                                                                                        con(self.__privacy_protocol, self.__authentication_protocol),
                                                                                                        self.__user_name, self.__privacy_password, self.__authentication_password,
                                                                                                        authentication_convertorC(self.__authentication_protocol),
                                                                                                        privacy_convertorC(self.__privacy_protocol),
                                                                                                        self.target_ip, self.port, self.name, self.value))
                models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = "OK")    

            # except :
            #     models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = "FAILED")
        #     errorIndication, errorStatus, errorIndex, varBinds = next(
        #         sendNotification(SnmpEngine(),
        #         UsmUserData(self.__user_name, authKey=self.__authentication_password, privKey=self.__privacy_password,
        #                     authProtocol=self.__authentication_protocol, privProtocol=self.__privacy_protocol, securityEngineId=self.__security_engine_id),
        #         UdpTransportTarget((self.target_ip, 162)),
        #         ContextData(contextEngineId=self.__context_engine_id, contextName=self.__context_name),
        #         'trap',
        #         NotificationType(ObjectIdentity(self.name)).addVarBinds((self.name, self.value))
        #         )
        #         )
        #     if errorIndication:
        #         models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = errorIndication)
        #     elif errorStatus:
        #         models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = '%s at %s' % (errorStatus.prettyPrint(),errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
                
        #     else:
        #         for varBind in varBinds:
        #             models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = "OK")
        # else:
        #     errorIndication, errorStatus, errorIndex, varBinds = next(
        #             sendNotification(
        #                 SnmpEngine(),
        #                 CommunityData(self.__community_string, mpModel=self.__mp_model),
        #                 UdpTransportTarget((self.target_ip, 162)),
        #                 ContextData(),
        #                 'trap',
        #                 NotificationType(ObjectIdentity(self.name)).addVarBinds((self.name, self.value))
        #                 )
        #             )
        #     if errorIndication:
        #         models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = errorIndication)
        #     elif errorStatus:
        #         models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = '%s at %s' % (errorStatus.prettyPrint(),errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
                
        #     else:
        #         for varBind in varBinds:
        #             models.SendTrapRecord.objects.create(Name = self.name, Value = self.value, SendTrapProfile = self.__profile, Response = "OK")    
            
                    
    