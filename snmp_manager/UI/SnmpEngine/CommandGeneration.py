from pysnmp.hlapi import *
from pysnmp.smi import builder, view, compiler, rfc1902
from ..models import PullingRecord, SnmpUsm, OIDTiming
from datetime import timedelta
def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def authentication_convertor(auth_protocol):
    if auth_protocol == "usmHMACMD5AuthProtocol":
        return usmHMACMD5AuthProtocol
    if auth_protocol == "usmHMACSHAAuthProtocol":
        return usmHMACSHAAuthProtocol
    if auth_protocol == "usmHMAC128SHA224AuthProtocol":
        return usmHMAC128SHA224AuthProtocol
    if auth_protocol == "usmHMAC192SHA256AuthProtocol":
        return usmHMAC192SHA256AuthProtocol
    if auth_protocol == "usmHMAC256SHA384AuthProtocol":
        return usmHMAC256SHA384AuthProtocol
    if auth_protocol == "usmHMAC384SHA512AuthProtocol":
        return usmHMAC384SHA512AuthProtocol
    if auth_protocol == "usmNoAuthProtocol":
        return usmNoAuthProtocol
    if auth_protocol == None:
        return None


def privacy_convertor(priv_protocol):
    if priv_protocol == "usmDESPrivProtocol":
        return usmDESPrivProtocol
    if priv_protocol == "usm3DESEDEPrivProtocol":
        return usm3DESEDEPrivProtocol
    if priv_protocol == "usmAesCfb128Protocol":
        return usmAesCfb128Protocol
    if priv_protocol == "usmAesCfb192Protocol":
        return usmAesCfb192Protocol
    if priv_protocol == "usmAesCfb256Protocol":
        return usmAesCfb256Protocol
    if priv_protocol == "usmNoPrivProtocol":
        return usmNoPrivProtocol
    if priv_protocol == None:
        return None


class CommandGenerators:
    def __init__(self, snmp_version, agent_IP, mib_OID, user_name=None, comminuty_string=None, privacy_password=None, privacy_protocol=None, authentication_password=None, authentication_protocol=None, new_value=None):
        self.userName = user_name
        self.setValue = new_value
        self.snmpVer = snmp_version
        self.privPass = privacy_password
        self.privProtocol = privacy_convertor(privacy_protocol)
        self.authPass = authentication_password
        self.authProtocol = authentication_convertor(authentication_protocol)
        self.ip = agent_IP
        self.mibOid = mib_OID
        self.comminutyString = comminuty_string

    def snmpGET(self):
        if self.snmpVer == "version3":
            errorIndication, errorStatus, errorIndex, varBinds = next(
                getCmd(SnmpEngine(),
                       UsmUserData(userName=self.userName,  authKey=self.authPass, privKey=self.privPass,
                                   authProtocol=self.authProtocol, privProtocol=self.privProtocol,
                                   securityEngineId=None,
                                   securityName=None),
                       UdpTransportTarget((self.ip, 161)),
                       ContextData(),
                       ObjectType(ObjectIdentity(self.mibOid)))
            )
            if errorIndication:
                return errorIndication
            elif errorStatus:
                return '%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
            else:
                for varBind in varBinds:
                    TTLRecord = OIDTiming.objects.filter(MibOID=self.mibOid)
                    if TTLRecord:
                        TTLRecord = OIDTiming.objects.get(MibOID=self.mibOid)
                        ttl = timedelta(TTLRecord.TimeToLive)
                    USMobject = SnmpUsm.objects.get(SnmpUserName=self.userName, SnmpVersion=self.snmpVer,
                                                    CommunityString=self.comminutyString, PrivacyPassword=self.privPass, AuthenticationPassword=self.authPass)

                    prePulling = PullingRecord.objects.filter(
                        SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    if prePulling:
                        prePulling = PullingRecord.objects.get(
                            SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                        prePulling.Value = varBind.prettyPrint()
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            prePulling.TimeToLive = timedelta(TTLRecord.TimeToLive)
                        prePulling.save()
                    else:
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            pulling = PullingRecord.objects.create(
                                SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject, TimeToLive=ttl)
                        else:
                            pulling = PullingRecord.objects.create(
                                SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    return ' = '.join([x.prettyPrint() for x in varBind])
        else:
            errorIndication, errorStatus, errorIndex, varBinds = next(
                getCmd(SnmpEngine(),
                       CommunityData(self.comminutyString),
                       UdpTransportTarget((self.ip, 161)),
                       ContextData(),
                       ObjectType(ObjectIdentity(self.mibOid)))
            )
            if errorIndication:
                return errorIndication
            elif errorStatus:
                return '%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
            else:
                for varBind in varBinds:
                    TTLRecord = OIDTiming.objects.filter(MibOID=self.mibOid)
                    if TTLRecord:
                        TTLRecord = OIDTiming.objects.get(MibOID=self.mibOid)
                        ttl = timedelta(TTLRecord.TimeToLive)
                    USMobject = SnmpUsm.objects.get(SnmpUserName=self.userName, SnmpVersion=self.snmpVer,
                                                    CommunityString=self.comminutyString, PrivacyPassword=self.privPass, AuthenticationPassword=self.authPass)
                    prePulling = PullingRecord.objects.filter(
                        SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    if prePulling:
                        prePulling = PullingRecord.objects.get(
                            SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                        prePulling.Value = varBind.prettyPrint()
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            prePulling.TimeToLive = ttl
                        prePulling.save()
                    else:
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            pulling = PullingRecord.objects.create(
                                SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject, TimeToLive=ttl)
                        else:
                            pulling = PullingRecord.objects.create(
                                SnmpMethod='get', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    return ' = '.join([x.prettyPrint() for x in varBind])

    def snmpSET(self):
        x = is_number(self.setValue)
        
        if x == True:
            self.setValue = int(self.setValue)
        print(self.setValue)
        if self.snmpVer == "version3":
            errorIndication, errorStatus, errorIndex, varBinds = next(
                setCmd(SnmpEngine(),
                       UsmUserData(userName=self.userName,  authKey=self.authPass, privKey=self.privPass,
                                   authProtocol=self.authProtocol, privProtocol=self.privProtocol,
                                   securityEngineId=None,
                                   securityName=None),
                       UdpTransportTarget((self.ip, 161)),
                       ContextData(),
                       ObjectType(ObjectIdentity(self.mibOid),
                                  self.setValue))
            )
            if errorIndication:
                return errorIndication
            elif errorStatus:
                return '%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
            else:
                for varBind in varBinds:
                    # TTLRecord = OIDTiming.objects.filter(MibOID=self.mibOid)
                    # if TTLRecord:
                    #     TTLRecord = OIDTiming.objects.get(MibOID=self.mibOid)
                    #     ttl = timedelta(TTLRecord.TimeToLive)
                    # USMobject = SnmpUsm.objects.get(SnmpUserName=self.userName, SnmpVersion=self.snmpVer,
                    #                                 CommunityString=self.comminutyString, PrivacyPassword=self.privPass, AuthenticationPassword=self.authPass)
                    # prePulling = PullingRecord.objects.filter(
                    #     SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    # if prePulling:
                    #     prePulling = PullingRecord.objects.get(
                    #         SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    #     prePulling.Value = varBind.prettyPrint()
                    #     if TTLRecord:
                    #         TTLRecord = OIDTiming.objects.get(
                    #             MibOID=self.mibOid)
                    #         ttl = timedelta(TTLRecord.TimeToLive)
                    #         prePulling.TimeToLive = ttl
                    #     prePulling.save()
                    # else:
                    #     if TTLRecord:
                    #         TTLRecord = OIDTiming.objects.get(
                    #             MibOID=self.mibOid)
                    #         ttl = timedelta(TTLRecord.TimeToLive)
                    #         pulling = PullingRecord.objects.create(
                    #             SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject, TimeToLive=ttl)
                    #     else:
                    #         pulling = PullingRecord.objects.create(
                    #             SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    return ' = '.join([x.prettyPrint() for x in varBind])
        else:
            errorIndication, errorStatus, errorIndex, varBinds = next(
                setCmd(SnmpEngine(),
                       CommunityData(self.comminutyString),
                       UdpTransportTarget((self.ip, 161)),
                       ContextData(),
                       ObjectType(ObjectIdentity(self.mibOid), self.setValue))
            )
            if errorIndication:
                return errorIndication
            elif errorStatus:
                return '%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
            else:
                for varBind in varBinds:
                    TTLRecord = OIDTiming.objects.filter(MibOID=self.mibOid)
                    if TTLRecord:
                        TTLRecord = OIDTiming.objects.get(MibOID=self.mibOid)
                        ttl = TTLRecord.TimeToLive
                    USMobject = SnmpUsm.objects.get(SnmpUserName=self.userName, SnmpVersion=self.snmpVer,
                                                    CommunityString=self.comminutyString, PrivacyPassword=self.privPass, AuthenticationPassword=self.authPass)
                    prePulling = PullingRecord.objects.filter(
                        SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    if prePulling:
                        prePulling = PullingRecord.objects.get(
                            SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                        prePulling.Value = varBind.prettyPrint()
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            prePulling.TimeToLive = ttl
                        prePulling.save()
                    else:
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            pulling = PullingRecord.objects.create(
                                SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject, TimeToLive=ttl)
                        else:
                            pulling = PullingRecord.objects.create(
                                SnmpMethod='set', AgentIP=self.ip, MibOID=self.mibOid, Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    return ' = '.join([x.prettyPrint() for x in varBind])

    def snmpGETNEXT(self, input_var):
        splited = input_var.split(" =")
        mib_symbol = splited[0]
        mib_identifier = [mib_symbol.split("::")[0]]
        object_symbol = mib_symbol.split("::")[1]
        object_path = object_symbol.split(".")
        complete_path = tuple(mib_identifier + object_path)
        mibBuilder = builder.MibBuilder()
        mibViewController = view.MibViewController(mibBuilder)
        objectIdentity = ObjectIdentity(*complete_path)
        objectIdentity.resolveWithMib(mibViewController)
        if self.snmpVer == "version3":
            errorIndication, errorStatus, errorIndex, varBinds = next(
                nextCmd(SnmpEngine(),
                        UsmUserData(userName=self.userName,  authKey=self.authPass, privKey=self.privPass,
                                    authProtocol=self.authProtocol, privProtocol=self.privProtocol,
                                    securityEngineId=None,
                                    securityName=None),
                        UdpTransportTarget((self.ip, 161)),
                        ContextData(),
                        ObjectType(ObjectIdentity(objectIdentity.getOid())))
            )
            if errorIndication:
                return errorIndication
            elif errorStatus:
                return '%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
            else:
                for varBind in varBinds:
                    TTLRecord = OIDTiming.objects.filter(MibOID=self.mibOid)
                    if TTLRecord:
                        TTLRecord = OIDTiming.objects.get(MibOID=self.mibOid)
                        ttl = timedelta(TTLRecord.TimeToLive)
                    USMobject = SnmpUsm.objects.get(SnmpUserName=self.userName, SnmpVersion=self.snmpVer,
                                                    CommunityString=self.comminutyString, PrivacyPassword=self.privPass, AuthenticationPassword=self.authPass)
                    prePulling = PullingRecord.objects.filter(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                    ), Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    if prePulling:
                        prePulling = PullingRecord.objects.get(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                        ), Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                        prePulling.Value = varBind.prettyPrint()
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            prePulling.TimeToLive = ttl
                        prePulling.save()
                    else:
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            pulling = PullingRecord.objects.create(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                            ), Value=varBind.prettyPrint(), SnmpUsm=USMobject, TimeToLive=ttl)
                        else:
                            pulling = PullingRecord.objects.create(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                            ), Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    return ' = '.join([x.prettyPrint() for x in varBind])
        else:
            errorIndication, errorStatus, errorIndex, varBinds = next(
                nextCmd(SnmpEngine(),
                        CommunityData(self.comminutyString),
                        UdpTransportTarget((self.ip, 161)),
                        ContextData(),
                        ObjectType(ObjectIdentity(self.mibOid)))
            )
            if errorIndication:
                return errorIndication
            elif errorStatus:
                return '%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
            else:
                for varBind in varBinds:
                    TTLRecord = OIDTiming.objects.filter(MibOID=self.mibOid)
                    if TTLRecord:
                        TTLRecord = OIDTiming.objects.get(MibOID=self.mibOid)
                        ttl = timedelta(TTLRecord.TimeToLive)
                    USMobject = SnmpUsm.objects.get(SnmpUserName=self.userName, SnmpVersion=self.snmpVer,
                                                    CommunityString=self.comminutyString, PrivacyPassword=self.privPass, AuthenticationPassword=self.authPass)
                    prePulling = PullingRecord.objects.filter(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                    ), Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    if prePulling:
                        prePulling = PullingRecord.objects.get(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                        ), Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                        prePulling.Value = varBind.prettyPrint()
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            prePulling.TimeToLive = ttl
                        prePulling.save()
                    else:
                        if TTLRecord:
                            TTLRecord = OIDTiming.objects.get(
                                MibOID=self.mibOid)
                            ttl = timedelta(TTLRecord.TimeToLive)
                            pulling = PullingRecord.objects.create(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                            ), Value=varBind.prettyPrint(), SnmpUsm=USMobject, TimeToLive=ttl)
                        else:
                            pulling = PullingRecord.objects.create(SnmpMethod='get', AgentIP=self.ip, MibOID=objectIdentity.getOid(
                            ), Value=varBind.prettyPrint(), SnmpUsm=USMobject)
                    return ' = '.join([x.prettyPrint() for x in varBind])
