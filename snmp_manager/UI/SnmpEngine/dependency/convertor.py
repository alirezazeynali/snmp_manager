from pysnmp.hlapi import *
def authentication_convertor(auth_protocol):
    if auth_protocol == "usmHMACMD5AuthProtocol":
        return usmHMACMD5AuthProtocol
    if auth_protocol == "usmHMACSHAAuthProtocol":
        return usmHMACSHAAuthProtocol
    if auth_protocol == "usmHMAC128SHA224AuthProtocol":
        return usmHMAC128SHA224AuthProtocol
    if auth_protocol == "usmHMAC192SHA256AuthProtocol":
        return usmHMAC192SHA256AuthProtocol
    if auth_protocol == "usmHMAC256SHA384AuthProtocol":
        return usmHMAC256SHA384AuthProtocol
    if auth_protocol == "usmHMAC384SHA512AuthProtocol":
        return usmHMAC384SHA512AuthProtocol
    if auth_protocol == "usmNoAuthProtocol":
        return usmNoAuthProtocol
    if auth_protocol == None:
        return None
def privacy_convertor(priv_protocol):
    if priv_protocol == "usmDESPrivProtocol":
        return  usmDESPrivProtocol
    if priv_protocol == "usm3DESEDEPrivProtocol":
        return  usm3DESEDEPrivProtocol
    if priv_protocol == "usmAesCfb128Protocol":
        return  usmAesCfb128Protocol
    if priv_protocol == "usmAesCfb192Protocol":
        return  usmAesCfb192Protocol
    if priv_protocol == "usmAesCfb256Protocol":
        return  usmAesCfb256Protocol
    if priv_protocol == "usmNoPrivProtocol":
        return  usmNoPrivProtocol
    if priv_protocol == None:
        return None
