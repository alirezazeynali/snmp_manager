from ... import models
from ..NotificationOrginator import *
import threading

class Checker(threading.Thread):
    def __init__(self, recieved_trap):
        threading.Thread.__init__(self)
        self.sender_ip = recieved_trap["SenderIP"]
        self.athentication = recieved_trap["Authentication"]
        self.engine_id = recieved_trap["EngineID"]
        self.context_engine_id = recieved_trap["ContextEngineID"]
        self.context_name = recieved_trap["ContextName"]
        self.oid_value = recieved_trap["OidValue"]
    def run(self):
        
        r_trap = models.RecievedTrapConfiguration.objects.get(SenderIP = self.sender_ip, SnmpUser = self.athentication, EngineID = self.engine_id)
        for key,value in self.oid_value.items():
                

                s_trap = models.SendTrapConfiguration.objects.filter(ObjectID = key, ContextEngineID = self.context_engine_id, RecievedTrap = r_trap)
                if s_trap:
                    
                    f_s_trap = models.SendTrapConfiguration.objects.get(ObjectID = key, ContextEngineID = self.context_engine_id, RecievedTrap = r_trap)
                    x = NotificationOrginator(f_s_trap, key, value, self.context_engine_id)
                    x.start()
                else:
                    pass